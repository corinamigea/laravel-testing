import unittest
import time

from selenium import webdriver
import string
import random
from selenium.webdriver.common.action_chains import ActionChains


class TestLaravel(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('C:\\Users\\corin\\PycharmProjects\\tools\\chromedriver.exe')
        self.driver.get('https://laravel.dev-society.com/')
    def tearDown(self):
        self.driver.quit()
    def test_Aboutpage(self):
        about_page = self.driver.find_element_by_xpath('//*[@id="about_page"]')
        about_page.click()
        time.sleep(1)
        self.assertIn('about', self.driver.current_url)
    def test_RegisterUser(self):

        register_button = self.driver.find_element_by_xpath('//*[@id="register_button"]')
        register_button.click()
        time.sleep(2)
        self.assertIn('register', self.driver.current_url)
        name_box = self.driver.find_element_by_xpath('//*[@id="name"]')
        name_box.click()
        name_box.send_keys('migea corina ana ')
        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        email_box.click()
        email_box.send_keys('corinamige@yahoo.com')
        password_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        password_box.click()
        password_box.send_keys('74tilisca')
        confirm_box = self.driver.find_element_by_xpath('//*[@id="password-confirm"]')
        confirm_box.click()
        confirm_box.send_keys('74tilisca')

    def user_login(self, user, password):
        login_page = self.driver.find_element_by_xpath('//*[@id="login_button"]')
        login_page.click()
        time.sleep(1)
        self.assertIn('login', self.driver.current_url)
        email_box = self.driver.find_element_by_xpath('//*[@id="email"]')
        pass_box = self.driver.find_element_by_xpath('//*[@id="password"]')
        email_box.clear()
        pass_box.clear()
        email_box.send_keys(user)
        pass_box.send_keys(password)
        login_button = self.driver.find_element_by_xpath('//*[@id="login_user"]')
        login_button.click()
        time.sleep(2)

    def test_ValidLogin(self):
        self.user_login('corinamige@yahoo.com','74tilisca')
        self.assertIn('dashboard', self.driver.current_url)
        logout_button = self.driver.find_element_by_xpath('//*[@id="sidebar-logout"]/a')
        time.sleep(2)
        logout_button.click()
    def test_InvalidLogin(self):
        self.user_login('corinamig@yahoo.com', '74tilisca')
        self.assertIn('These credentials do not match our records.', self.driver.page_source)

        self.user_login('corinamige@yahoo.com', '74tilisc')
        self.assertIn('These credentials do not match our records.', self.driver.page_source)

    def goto_PostsPage(self):
        posts_button = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        posts_button.click()
        time.sleep(1)
        self.assertIn('posts', self.driver.current_url)

    def test_1_AddNewPost(self):
        self.user_login('corinamige@yahoo.com', '74tilisca')
        self.assertIn('dashboard', self.driver.current_url)
        new_post = self.driver.find_element_by_xpath('//*[@id="sidebar-posts"]/a')
        new_post.click()
        time.sleep(1)
        self.assertIn('posts', self.driver.current_url)
        add_new = self.driver.find_element_by_xpath('//*[@id="create_post"]')
        add_new.click()
        time.sleep(1)
        post_title = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')

        post_title.send_keys('scrie ceva')
        time.sleep(1)

        body_text = self.driver.find_element_by_xpath('//*[@id="text_editor"]')
        body_text.send_keys('abcdefghi')

        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()
        time.sleep(1)

        self.assertIn('Post created', self.driver.page_source)

        a = self.driver.current_url
        lista = a.split('/')
        TestLaravel.ID = lista[-1]
        print(TestLaravel.ID)

    def test_3_DeleteComment(self):
        self.user_login('corinamige@yahoo.com', '74tilisca')
        self.assertIn('dashboard', self.driver.current_url)
       # // *[ @ id = "delete-post-28"]
        xpath = '// *[ @ id = "delete-post-'+ TestLaravel.ID +'"]'
        delete_button = self.driver.find_element_by_xpath(xpath)
        delete_button.click()
        time.sleep(2)
    def test_2_edit(self):
        self.user_login('corinamige@yahoo.com', '74tilisca')
      # // *[ @ id = "edit-post-21"]
        xpath = '// *[ @ id = "edit-post-'+ TestLaravel.ID +'"]'
        edit_button = self.driver.find_element_by_xpath(xpath)
        edit_button.click()
        time.sleep(2)
        edit_title = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/input[2]')
        edit_title.clear()
        edit_title.send_keys('zoro')

        time.sleep(2)
        submit_button = self.driver.find_element_by_xpath('//*[@id="submit_post"]')
        submit_button.click()
        time.sleep(2)
    def test_ValidAdmin(self):

        self.user_login('corinamige@yahoo.com', '74tilisca')
        self.assertIn('edit', self.driver.current_url)
        drop_down = self.driver.find_element_by_xpath('/html/body/main/div/div/main/form/select')

        hover = ActionChains(self.driver).move_to_element(drop_down)
        hover.perform()
        time.sleep(3)

        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()


    def test_settings(self):
        self.user_login()


        settings = self.driver.find_element_by_xpath('//*[@id="sidebar-settings"]/a')
        settings.click()

        edit_button = self.driver.find_element_by_xpath('//*[@id="edit_account"]')
        edit_button.click()
        self.assertIn('edit', self.driver.current_url)

        change_user = self.driver.find_element_by_xpath('//*[@id="user"]')
        change_user.click()
        time.sleep(1)
        select_admin = self.driver.find_element_by_xpath('//*[@id="admin"]')
        select_admin.click()
        time.sleep(1)
        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()
        time.sleep(1)
    def test_editusers(self):
        self.user_login()
        users = self.driver.find_element_by_xpath('//*[@id="sidebar-users"]/a')
        users.click()


        self.assertIn('users',self.driver.current_url)
        edit_users = self.driver.find_element_by_xpath('//*[@id="edit-user-20"]')
        edit_users.click()

        name_box = self.driver.find_element_by_xpath('//*[@id="edit_name"]')
        name_box.clear()
        name_box.send_keys('')
        time.sleep(2)
        submit_button = self.driver.find_element_by_xpath('//*[@id="apply"]')
        submit_button.click()
    def test_deleteuser(self):
        self.user_login()
        settings = self.driver.find_element_by_xpath('//*[@id="sidebar-users"]/a')
        settings.click()
        self.assertIn('settings', self.driver.current_url)
        delete_user = self.driver.find_element_by_xpath('//*[@id="delete_button"]')
        delete_user.click()
        time.sleep(1)
                   





    def main():
        test_suite = unittest.TestSuite()
        test_suite.addTest(TestLaravel('user_login'))

    def main(test_plan):
    test_suite = unittest.TestSuite()
    fisier = open(test_plan, 'r')
    lines = fisier.read().split('\n')
    for line in lines:
        test_suite.addTest(TestLaravel(line))
    runner = unittest.TextTestRunner(verbosity=3)
    runner.run(test_suite)
main(sys.argv[1])












