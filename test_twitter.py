import unittest
import time
from selenium import webdriver
class TestTwitter(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('C:\\Users\\corin\\PycharmProjects\\tools\\chromedriver.exe')

        self.driver.get('https://twitter.com')
    def test_login(self):
        login_page = self.driver.find_element_by_xpath('//*[@id="doc"]/div/div[1]/div[1]/div[2]/div[2]/div/a[2]')

        login_page.click()
        time.sleep(1)
        self.assertIn('login', self.driver.current_url)

        time.sleep(1)

        email_box = self.driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/fieldset/div[1]/input')
        pass_box = self.driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/fieldset/div[2]/input')

        email_box.clear()
        pass_box.clear()

        email_box.send_keys('george@azimutvision.ro')
        pass_box.send_keys('cursuriazimut')

        login_button = self.driver.find_element_by_xpath('//*[@id="page-container"]/div/div[1]/form/div[2]/button')
        login_button.click()
        time.sleep(2)

